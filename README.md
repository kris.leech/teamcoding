## Teamcoding Website

[Teamcoding.com](https://teamcoding.com)

* [Gitlab](https://gitlab.com/kris.leech/teamcoding.git)
* [Hugo](https://gohugo.io/)
* [m10c theme](https://github.com/vaga/hugo-theme-m10c)
* [Render](https://render.io)
