---
title: "Activerecord SQL"
date: 2019-01-09
---

We want to query out database, but not generate ActiveRecord models (which takes additional resources).

Since this report displays a LOT of rows of data we are aiming for speed.

```ruby
sql = Recruitment::Patient.order(reference: :desc).select(patient_attrs).to_sql

results = ActiveRecord::Base.connection.execute(sql).map do |row|
  Hash[patient_attrs.zip(row)].transform_values(&method(:format_value))
end

def patient_attrs
  @patient_attrs ||= %i(identifier study_reference consented_on screened_on recruited_on)
end

def format_value(value)
  # ...
  value
end
```

`results` will be an `Array<Hash>`. You could make it a `Array<Struct>` if prefered.

