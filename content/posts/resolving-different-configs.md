---
title: "Resolving Different Configs"
date: 2019-08-10
draft: true
---

These are the possible configurations we want to support:

```ruby
config = :invited_on

config = [:invited_on, :started_on]

config = lambda { |s| s.started_on }

config = ResolveValue.new # object which `responds_to?(:call)`
```

We have an object from which we want to extract a value:

```
model = MyModel.first
```

If the config is a Symbol (or String) we can use `send`:

```ruby
value = model.send(config)
```

This is how we can support a Symbol and Array:

```ruby
value = case config
when Symbol
  model.send(config)
when Array
  config.lazy.map { |it| model.send(it) }.find { |value| !value.nil? }
else
  raise "Unsupported mapping type: #{config.class.name}"
end
```

Note: We shouldn't use `case config.class` because `case` uses `===` and `Symbol === :foo # => true`. Whereas `Symbol === Symbol # => false`.

Note: I use `lazy` because in my case the method calls where expensive, so once a non-nil value is found it will stop and return the value instead of mapping the entire collection before passing to `find`.

We could in theory turn the Symbol in to an Array and use the same bit of code, worth it?

```ruby
value = Array(config).lazy.map { |it| model.send(it) }.find { |value| !value.nil? }
```

Anyway, to support the lambda which is a Proc instance (`lambda {}.class # => Proc`):

```ruby
value = case config
when Symbol
  model.send(config)
when Array
  config.lazy.map { |it| model.send(it) }.find { |value| !value.nil? }
when Proc
  config.call(model)
else
  raise "Unsupported mapping type"
end
```

This all works well, but does not support non-Proc callables, e.g. an object which `respond_to?(:call)`.

We can't use the `case` anymore, the class of a callable could be anything.

Let's reverse the order we support the various configuration options and start with callables:

```ruby
value = config.call(model)
```

Now how would be support a Symbol, or Array of Symbol?

We could make it in to a callable...

```ruby
config = lambda { |it,config| it.send(config) } if config.is_a?(Symbol)

config.call(model)
```

Or we could just branch if the given config is callable:

```ruby

value = if config.respond_to?(:call)
    config.call(model)
  else
    case config
    when Symbol
      model.send(config)
    when Array
      config.lazy.map { |it| model.send(it) }.find { |value| !value.nil? }
    else
      raise "Unsupported mapping type"
    end
  end
```

All in all I might favour something like:

```ruby
value = if config.respond_to?(:call)
    config.call(model)
  else
    Array(config).lazy.map { |it| model.send(it) }.find { |value| !value.nil? }
  end
```  

This by chance now also allows `config` to a String since it too can be passed to `send`.

While this will still raise, with a `TypeError`, if we pass something other than a callable, Symbol or String, we don't get the specific error message explaining what happened. But then maybe the config should already have been validated to make sure it is of the correct type. If we validate types/format etc. on the outside (edges/boundaries) we can assume we have the correct types/formats on the inside (the core).

