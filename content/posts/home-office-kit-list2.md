---
title: "Home Office Kit List 2018"
date: 2018-05-18
---

## Hardware

* ActivForce Electric Standing Desk

I can sit or stand at this desk with the flick of a button. I picked this on ebay real cheap

* Dell Ultra wide U3415Wb

This is the first time I have used either a HD monitor or an ultrawide. I
prefer this over two seperate monitors.

* Code Keyboard Version 2

Mechanical keys, nice and clicky, great feel, keys light up if need. Small
form, no numeric pad, so less distence to reach for the mouse.

I really like the Microsoft ergonmic keyword too, but it is far to wide
and requires too much learning to reach the mouse.

* Amazon Basic adjustable monitor stand

This is a very recent purchase and replace a stack of books which my monitor
preceriously balanced on. I have the version which clamps to the back of my
desk.

* Slient PC

Paying the extra for a high spec PC is a sound investment, I'd say out of all
the hardware this was the best bang for your pound. No waiting, spinners etc.
It runs Windows 10 in VirtualBox with no lag or noticeable perfomance issues.

* Steelcase chair with arm rest removed

I try and stand but when I need to sit this is a nice chair, I picked it up
second hand on ebay. I removed the arm rests as they just get in the way. I
picked this chair as it was recommended by Avdi Grimm.

* Wireless mouse

This is a new mouse, which replaced the 3M... which I need to fix after our
puppy chewed through the cable.

## Software

* Ubuntu LTS

I tried Debian too, but I find Ubuntu much easier to just getting going with,
particually the PPA's for installing software not in the offical repo's.

* i3 tiling window manager

This is proberbly the thing I couldn't live without now. I used to spend too
much time lining windows up on the left and right. Now I just hit a shortcut
starting typing in the name of an app and it opens perfectly in a split screen.
The splits can be resized, tho I rearely do this. Usually I have a 50/50 split
with a browser and other apps stacked on one side and a bunch of terminals on
the other.

* NeoVim
* Git
* Thunderbird
* Firefox
* Libre Office
* Dropbox (most of the home directory is symlink to Dropbox)
* pass + qtpass for managing passwords stored on Dropbox.

