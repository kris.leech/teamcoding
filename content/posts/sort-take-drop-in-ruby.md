---
title: "Sort, take and drop in Ruby"
date: 2018-09-18
---

## drop and take

```ruby
[1,2,3,4,5,6,7].drop(2) # => [3,4,5,6,7]

[1,2,3,4,5,6,7].take(2) # => [1,2]

[1,2,3,4,5,6,7].reverse.take(2) # => [5,7]

[1,2,3,4,5,6,7].reverse.drop(2) # => [5, 4, 3, 2, 1]
```

# sorting asc and desc

```ruby
[1,2,3].shuffle.sort == [1,2,3] # => true

[1,2,3].shuffle.sort.reverse == [3,2,1] # => true

[1,2,3].shuffle.sort { |a,b| a <=> b } == [1,2,3].shuffle.sort # => true

[1,2,3].shuffle.sort { |a,b| b <=> a } == [1,2,3].shuffle.sort.reverse # => true

reverse_order = lambda { |a,b| b <=> a }

[1,2,3].shuffle.sort(&reverse_order) == [1,2,3].shuffle.sort.reverse # => true
```

# getting N smallest numbers

```ruby
n = 5

[1,2,3,4,5,6,7].shuffle.sort { |a,b| b <=> a }.drop(n) # => [5,4,3,2,1]
```

Note in the above we use `b <=> a` to sort in reverse order.

