---
title: "Call and Curry in Ruby"
date: 2019-06-17
---

curry and callables

```ruby
add = Proc.new { |a,b| a+b }
add.call(1,2) # => 3

add_one = add.curry.call(1)
add_one.call(2) # => 3
```
 
Since a method is also callable we can do the following:
 
```ruby
def subtract(a,b)
  b - a
end
 
subtract_one = method(:substract).curry.call(1)

subtract_one.call(5) # => 4
```

