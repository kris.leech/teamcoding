---
title: "Ruby Double"
date: 2019-02-03
---

An implementation of a double in Ruby.

```ruby
class Double < Hash
  def initialize(attrs)
    super
    attrs.each { |k,v| store(k,v) }
  end

  def method_missing(_name, *args, &block)
    has_key?(_name) ? fetch(_name) : super(_name, *args, &block)
  end
end

property = Double.new(title: "The Stanley Hotel", number_of_past_guests: 5)

propery.id # => 1
```
