---
title: "Ruby Dockerfile"
date: 2019-10-26
---

Ruby 2.6 on Ubuntu with Bundler 2.x

```
# Dockerfile

FROM ubuntu

RUN apt-get update
RUN apt-get install -y build-essential wget gcc automake autoconf autogen libtool make cmake bison unzip flex libqt4-dev zlib1g zlib1g-dev libqtwebkit-dev libreadline6 libreadline-dev  libreadline6-dev libssl-dev libfontconfig1-dev chrpath mysql-client libxml2-dev libxslt1-dev libxslt-dev libmysqlclient-dev libsqlite3-dev git git-core xvfb socat libyaml-dev libgdbm-dev libncurses5-dev libffi-dev

# Install Chrome, used headless for feature specs
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
RUN sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN sudo apt-get update
RUN sudo apt-get install -y google-chrome-stable

ADD rootfs/etc /etc/
ADD rootfs/root /root/
ADD rootfs/home/ubuntu /home/ubuntu/

RUN wget --no-check-certificate -O ruby-install.tar.gz https://github.com/postmodern/ruby-install/archive/master.tar.gz
RUN tar -xzvf ruby-install.tar.gz
RUN cd ruby-install-master && make install
RUN cd /
RUN rm -rf ruby-install-master && rm -rf ruby-install.tar.gz

RUN ruby-install --latest
RUN ruby-install -i /usr/local/ ruby 2.6.0 -- --disable-install-doc
RUN gem update --system --no-document
RUN gem install bundler --force
```

```
docker build --tag ruby:2.6.0 .
docker run -i -t --entrypoint /bin/bash ruby:2.6.0
docker run -i -t -v ~/dev/project:/project --entrypoint /bin/bash ruby:2.6.0
```




