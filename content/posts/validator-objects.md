---
title: "Validator Objects"
date: 2018-10-18
draft: true
---

```ruby
class Form
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  include Virtus.model
      
  attribute :performer
  attribute :site_identifier, String
  attribute :study_id, Integer
      
  validates :performer,       presence: true, strict: true
  validates :study_id,        presence: true, strict: true
  validates :site_identifier, presence: true

  def initialize(attributes)
    super(attributes.to_h)
  end

  def study
    @study ||= Study.find!(study_id)
  end
end
```

# In controller

```ruby
form = Form.new(form_params)
command.call(form)
```

Within Form we are hardcoding, and thus can not inject, Study. This means we can't test Form in isolation, even worse since
we are using ActiveRecord we need a database connection (or stub calls to `#find`).

It would be prefrable to leave `#initialize` as dependency injection and have a seperate method, `#call` to process the inputs. The trouble 
with this is the form is doing more than one thing. It is both acting as a form, something which has attributes and conforms the ActiveModel API and
validating user input and producing errors.

What if the object which we passed to `form_for` was a simple Struct with ActiveModel complience. We then had a seperate validation object to
which we can pass the Struct object.

```ruby
class Form < Struct
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming
end

class Validator
  def  initializer(dependencies = {})
    @study_repo = dependencies.fetch(:study_repo) { Study }
  end

  def call(form)
    # return errors
  end
end


class Command
  def call(form)
    result = Validator.call(form)
    if result.valid?
      # save some data
    else
    end
  end
end
```

This has another bonus we don't need to keep making validator objects, we can initialize it once and reuse the same object since it has no
state.

```ruby
Container = {}
Container[:validator] = Validator.new
```
