---
title: "S3 Ruby"
date: 2017-06-19
---

```ruby
client = Aws::S3::Client.new(region: 'eu-west-2',
                             access_key_id: '...',
                             secret_access_key: '...')

# buckets are top-level, there are no sub-buckets and must be unique

client.create_bucket(bucket: 'acme-test', acl: 'private')

client.delete_bucket(bucket: 'acme-test')
                                         
# delete folders/files
# if the folder is not empty it will remain until all contents are deleted.
client.delete_object(bucket: 'acme-test', key: 'documents/')

# create folder (key must end in a slash)
client.put_object(bucket: 'acme-test', key: 'documents/')

# upload a file
client.put_object(bucket: 'acme-test', key: 'documents/my.csv', body: File.read(path_to_csv_file))

client.put_object(bucket: 'acme-test', key: 'documents/my.csv', body: File.read(path_to_csv_file), metadata: { "hostname" => 'example.com' })

# delete everything in a folder (and sub-folders)

client.list_objects(bucket: 'acme-test', prefix: 'one/').contents.each do |obj|
  client.delete_object(bucket: 'acme-test', key: obj.key)
end
```

