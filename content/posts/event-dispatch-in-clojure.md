---
title: "Event Dispatch in Clojure"
date: 2018-04-16
---

Generic function which take a handler function and calls it passing in a event map. Typically this will be used with partial to create a 
dispatch function which is bound to a handler.

```clojure
(defn dispatch [handler message] 
  (let [event (json/read-str message)]
    (println "<- dispatch" event "to" handler) 
    (handler event)))
```    

A handler, this just prints the event

```clj
(defn event-handler [event] (println event))
```

A function which wraps our handler, allowing us to pass a raw JSON message, but the handler gets an event map.

```clj
(def event-dispatch (partial dispatch event-handler))
```

This is how it is called, we would typically hook this up to a stream of JSON events coming from a message bus or websocket.
```clj
(def json (json/write-str { :name "hello" }))

(event-dispatch json)
```

