---
layout: post
title: "Random Links"
date: 2013-08-07
comments: true
categories: [random]
draft: true
---

* http://learnxinyminutes.com/docs/clojure/
* http://postbox-inc.com/?/blog/entry/email_filtering_strategies/
* https://gist.github.com/jbr/823701
* http://www.vim.org/scripts/script.php?script_id=2425
