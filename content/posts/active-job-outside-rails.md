---
title: "Active Job Outside Rails"
date: 2014-10-07
---

```ruby
require 'sidekiq'
require 'active_job'

ActiveJob::Base.queue_adapter = :sidekiq

class Foo < ActiveJob::Base
  queue_as :default

  def perform
    sleep 10
  end
end

Foo.perform_later # => undefined method 'perform_later'
```

