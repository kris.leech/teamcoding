---
title: "Versioned_classes"
date: 2019-10-25
---

In Rails migrations now need a version number:

```ruby
class ActiveRecord::Migration[5.2]
  # ...
end
```

I haven't looked at the code in Rails for this, but one way it could be done would be as such:

```ruby
module ActiveRecord
  class Migration
    def self.[](version)
      const_get(version.to_s.split('.').map { |num| 'V' + num }.join('::'))
    end
    
    module V5
      class V2
      end
    end
    
    module V4
      class V0
      end
    end
  end
end

ActiveRecord::Migration[5.2] # => ActiveRecord::Migration::V5::V2

class MyMigration < ActiveRecord::Migration[5.2]
  # ...
end
```


