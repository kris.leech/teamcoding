---
title: "From Vim Novice to Proficient in 9 months"
date: 2013-10-12
---

## From Vim Novice to Proficient in 9 months

I recently read a book called Mastery which sparked the writing of this blog post.

Why Vim?

I remember joking once with a fellow coder on seeing they used Vim that is was a bit old school. I was nieve to the effientcy that could be gleaned from finger tip manipulation of text. In fact my only use of text editors was Notepad, TextEdit and pico. My attitude was why use something as plain and boring as a text editor when you can have an IDE (Integrated Development Environment) - buttons, wizards and things to click. 

In hindsight this is why Vim was a good choice:

* Great support for Unix tools such as ack and ctags
* Cross platform same experience on MacOS (development) and Linux (servers)
* Terminal based so works on a server, think sysadmin and remote pair programming

Life before Vim

I had been using Netbeans for a few years, when in dropped support for Ruby I went looking for a new IDE. I'd preciously used Eclipse, RadRails and Aptana on and off over the last 8 or so years. I also dabbled with the commercial RubyMine editor. 

Around the same time I was suffering from RSI pretty bad in my arms and hands, so much so I had to take time of work and start physio sessions.

I'd read about coders I respected using vim - I decided to take a look. What appealed was the mouselessness of vim. My RSI was largely caused my incorrect posture and the use of a mouse. To this day I can't use a convensional mouse for more than an hour or so.

Pre-Journey Warning

There is a steep learning curve. Expect your productivity to drop for a while. Expect to be frustrated. Expect to make progress and then find yourself taking two steps backwards.

Novice

Install Vim with Ruby support: https://github.com/krisleech/vimfiles

start Vim

$ vim

(vi is usually symlinked to vim as well)

The first thing you need to do is type "i" to enter insert mode. Now bash some keys, see it works like a regular text editor.

Now press Esc to enter command mode and use w and b to move forward a word and backwards a word. Use hjkl to move the cursor left, down, up and right.

Now read this: http://yehudakatz.com/2010/07/29/everyone-who-tried-to-convince-me-to-use-vim-was-wrong/

This gist is many Vim pro's will tell you turn off the mouse and not to use the arrow keys, don't take this adviceÉ Instead start using Vim like your previous editor and gradually integrate vimisms.

To turn on the mouse, in command mode (Esc):

:set mouse=a

    Understand insert, visual and command mode: http://blog.interlinked.org/tutorials/vim_tutorial.html
    Start with basic movements: hjkl (or arrow keys), w/W, b/B

Vim has two main modes, command and insert. Insert mode acts like a regular text editor; what ever you type appears on the screen. In command mode you can move the cursor and manipulate your text.

Print this: http://walking-without-crutches.heroku.com/image/images/vi-vim-cheat-sheet.png

Learn what each key on the keyboard does in command mode
Learn to use the help
:help text-notations

Use plugins such as NerdTree and Command-T

Early on a kept flicking back to Netbeans or RubyMine as I had to get work done. At some point I decided to go cold turkey. You have to go cold turkey if your ever going to make it past the first hurdle.

Once your comfortable switching between insert and command modes and using w and b to skim over words think about turning off arrow keys and using hjkl - again go cold turkey.

Terminology:

Frame - the outer most box in which vim is contained
Buffer - text in memory, i.e an unsaved file
Window - you can split the frame in to multiple windows (e.g test and model side by side)
File - text on disc, i.e a saved file


GUI Vim verus Terminal Vim

I started with GUI Vim, MacVim specifically. The advantage of GUI Vim is copy/paste is a lot easier. However, nowadays I use Vim in a terminal but only because I use tux to manage my windows instead of GUI tabs. I would strongly recommend not learning tmux at the same time as Vim.

TOP TIPS:

- 
- hand write your own cheat sheet (keep it small and evolve it as key combinations become second nature)
- get inspired by watching other vim users

Advanced Beginner

This is the point at which I leaned the most and took the longest.

Don't try and learn everything at once, build your memory slowly. Smalls wins add up.
Start using more single character a-z commands (cheatsheet firmly bluetacked to wall)
e.g. Using o / O to insert new line. Try to incorp[erate one new command at a time and once committed to memory move on to the next.
Right on a piece of paper 5 or so commands you are trying to learn and tape it to the bottom of your monitor.
Be prepared for a drop in productivity and some hard moments where it might not seem like you are making any progress, or in fact things are not progressing as quickly as your might have liked. Do not be disheartened.

Master yank(copy) / paste early on, this can take a while.

Master the surround plugin, this will also teach you about text-objects and motions
For example: 

ysW"  - surround word in quote marks
*hello world  => "hello world"

ysiW" - surround inner word in quote marks
hell*o world => "hello world"



:help text-objects

If you are doing something slowley, work out how to do it faster. For example do you find yourself holding the j (or down arrow key) to scroll down the page? Try the following:

} - down one paragraph (block of code)
M - jump to middle of screen
:32 - jump to line 32
G32 - jump to line 32


Read. Watch vimcasts.
IRC freenode#vim
IRC freenode#macvim
You should be spending most time in command mode.
Get in to the habit of hitting Esc (or jj) after you have finished typing.

Learn visual mode:

V enters visual mode, then use jk to select lines.
Find/Replace in visual selection:
:s/@/let/gg
d - delete selection
c - change selection

vir - visual inner ruby; selects inner block of ruby (screen shot)
vor - visual outter ruby

once selected press ir or or to expand/contract the selection further.

*** Competent

Customisation in .vimrc (start your own vimrc file from scratch, only include plugins you use.)
vimrc is personal, one size does not fit all.
Plugin manager (I use Vundle, Pathogen is also very popular.)
Learn about mappings.
Understand what each line in .vimrc does.
  http://stevelosh.com/blog/2010/09/coming-home-to-vim/#important-vimrc-lines   

  My vimfile: 

At this point I also decided to drop MacVim and just use terminal Vim. Why? Because it means everything in one terminal (server, logs, console etc.) so less windows flipping.

100% Vim
Watch screencasts, hang out in IRC
Install Vim 7.3 and my vimfiles on all my servers - this is great! I put my vimrc on Github and deployed it my servers. Its a nice experience to have the same environment on desktop and server.

In fact I looked at the possibility of having my entire development environment on a server and the desktop would simply act as a thin client. SSH inÉ Everything how I left it.

Ditch NerdTree and start using Ctrl+P or Command-T.

Proficient (it took 9 months to get here)

This is where I'm at. No arrow keys, no mouse. 100% vim. 

I have my own vimfiles online, publish them. Don't worry too much about feedback - vimrc is personal.

I also started using tmux to manage tabs instead of using GUI tabs (ITerm2 in my case).

Vim everywhere - key binding for other apps (not usually very good, try Commando for a better experience.) Write blog posts, emails etc. in vim. If you find yourself typing jjjj when writing an email thats a good sign!

Spread the word - but remember its no silver bullet. Don't be arrogant. Varity is the spice of life.

Keyboards:

The Apple keyboard is terrible - it gave me the worst RSI.
Blank DAS
TypeMatrix
The Microsoft Natural Ergonomic Keyboard 4000 - The best if it where short form for when I did need to use a mouse.
Happy Hacker

EXPERT

Whats next? ExpertÉ I feel this is a big jump and I'm pretty much cursing at a nice skill level where I much more efficient than I was before I started using vim. So this is the hardest jump to make as I feel like I don't need to make it in practice. 

Pair programming remotely using Vim.
Using Marks (I never use them)
I've never used Macro's either.
Touch typing, properly.
Keep learning! Think. Take the time to do things better (be a novice again).
Try to change some habits such as using jj instead of Esc to switch to command mode, and using ,w instead of :w to save.  <- done.
Try EmacsÉ Seriously - what can I learn from emacs, might I like it better, will I loose my mustle memoryÉ

Its difficult when your in the flow of coding to stop and work out how to do something better in vim, but if you can make it a habit the numbers of times you have to stop gets less and less. In the short term you loose a little, in the long term you gain a lot.

