---
layout: post
title: "Getting vimux to use bundler"
date: 2014-01-07
comments: true
published: true
categories: [tmux, rspec]
---

```
let g:turbux_command_rspec  = 'bundle exec rspec'
```
