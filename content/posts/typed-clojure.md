---
title: "Typed Clojure"
date: 2018-10-04T09:24:32+01:00
---

A small example of adding types to Clojure using [Typed
Clojure](https://typedclojure.org/).

Use `t/ann` to annotate a var with a type definition.

```clj
(t/ann stringy [t/Str -> t/Str]))

(defn stringy [a] (str a))
```

In the above case we are
assigning a function to the var `stringy` and the inputs and outputs are type
checked.

```clj
(stringy "1") ;; => "1"

(stringy 1) ;; => runtime type error
```

Compile error occur if the return value is known to be incorrect.

```clj
(defn stringy [a] (symbol a)) ;; => compile time error
```

This works for an var, not just functions:

```clj
(t/ann person '{:name t/Str})
(def person { :name 1 }) ;; => error
```

