---
title: "Guarding Config Type"
date: 2016-01-22
---

```ruby

require 'dry/container'
require 'logger'

module Oxygen
  class Configuration
    extend Dry::Container::Mixin

    # default configuration
    register('reports', [])
    register('utils.logger', Logger.new($stdout))

    module Reports
      def self.register(configuration)
        raise ArgumentError unless %i(name url section).all? { |key| configuration.keys.include?(key) }
        Configuration['reports'] << configuration
        self
      end

      def self.all
        Configuration['reports']
      end
    end
  end
end

Oxygen::Configuration::Reports.register(name: '...', url: '...', section: '...')
Oxygen::Configuration::Reports.all # => [...]
Oxygen::Configuration['reports'] # => [...]
```

