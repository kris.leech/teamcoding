---
title: "Inject Time as a dependency in Ruby"
date: 2018-10-12
---

By allowing us to inject a dependency which is mutable:

```ruby
class SendReport
  def initialize(dependencies = {})
    @clock = dependencies.fetch(:clock) { Date }
  end

  def call
    # ...
  end

  private

  def today
    @clock.today
  end
end
```

We can stub it in our specs:

```ruby
describe '#call' do
  let(:today)  { Date.parse('20/08/2018') }
  let(:clock)  { double('Clock', today: today) }

  it 'generates report up to last month' do
    subject = described_class.new(clock: clock)
    # ...
  end
end

```

