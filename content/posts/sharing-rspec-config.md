---
title: "Sharing Rspec Config"
date: 2017-10-23
---

Sharing Standard RSpec configuration between multiple projects.

In our example we have a gem called Oxygen which contains all non-framework code shared between all the codebases (repo's) which make up our system.

```ruby
module Oxygen
  module RSpec
    def self.configure!
      ::RSpec.configure(&Configuration)
    end

    # Standard RSpec configuration
    #
    # @example
    #  RSpec.configure(&Configuration)
    #
    Configuration = lambda do |config|
      config.warnings = true
      config.example_status_persistence_file_path = "/tmp/core_example.txt"

      # disable `should` syntax
      config.expect_with :rspec do |c|
        c.syntax = :expect
      end

      config.mock_with :rspec do |c|
        c.syntax = :expect
      end

      config.filter_run :focus => true
      config.run_all_when_everything_filtered = true
    end
  end
end
```

You can use this in `spec_helper` with:

```ruby
require 'oxygen/rspec/configuration'
Oxygen::RSpec.configure!
```

