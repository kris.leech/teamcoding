---
title: "Vim Registers"
date: 2017-11-14
---

Usually you would yank some text and then paste it elsewhere, as such:

`y$` (yank to end of line)
`p` (paste text)

But what if, between these two commands you need to delete text, e.g. `dd`, which will mean `p` will paste the text just deleted, not that which was yanked initially.

Instead of plain yank, yank to a register:

```
the quick brown fox
```

`"kY` (in register `k` yank line `Y`). You can also do the other usual types of yank, e.g. `y$` (yank to end of line), `yW` (yank word) etc.

Note: `k` is arbitrary and can be any letter.

You can now delete some lines of text, `dd`, `dW` etc., and otherwise edit the text.

And then get back the text yanked in to register `k` with:

`"kp` (from register `k` paste)

I find it useful to map this on the fly (to avoid cluttering up my vimrc):

```
:map ,yy "kY
:map ,pp "kp
```

`,yy` will yank a line to register `k` and `,pp` will paste a line from register `k`.

Another method is to paste from register `0` which always contains the last yanked item (i.e. unlike plain `p` does includes deleted text).

`"0p`

Again you could map this:

`:map ,pp "0p`


Then I just yank, do some editting then `,pp` to paste the last yanked text.

