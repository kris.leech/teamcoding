---
title: "Using Jxls to create Excel files in Clojure"
date: 2018-12-14
---

[Jxls](http://jxls.sourceforge.net/) is a Java library used for the generation of Excel reports. Jxls uses a special markup in Excel templates to define output formatting and data layout.

We are going to use [Java interop](https://clojure.org/reference/java_interop) to call the library from Clojure.

```clj
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.jxls/jxls "2.4.2"]
                 [org.jxls/jxls-poi "1.0.13" :exclusions [org.jxls/jxls]]])

(import [org.jxls.util JxlsHelper])
(import [org.jxls.common Context])

(require '(clojure.java [io :as io]))

;; http://jxls.sourceforge.net/xls/object_collection_template.xls
(def io (io/input-stream "object_collection_template.xls"))
(def os (io/output-stream "out.xls"))

;; keys and values must be strings, they are formatted as per template.
(def employees [ { "name" "Kris" } { "name" "Chris" } ])

(def context (Context.))
(.putVar context "employees" employees)

(def helper  (JxlsHelper/getInstance))

(.processTemplate helper io os context)
```

If you try and do `.processTemplate` twice with the same io stream you get `java.lang.IllegalStateException: Cannot load XLS transformer. Please make sure a Transformer implementation is in classpath`.

