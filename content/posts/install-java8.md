---
title: "Install Java8 for an old version of ElasticSearch"
date: 2018-06-06
---

Installing an old version of ElasticSearch 1.7 which requires Java 8 for
development.

## Install Java 8

To install without clobbering your existing version use [sdkman](https://sdkman.io).

```
export SDKMAN_DIR="$HOME/bin/sdkman"
curl -s "https://get.sdkman.io" | bash
exec $SHELL
```

Then install your chosen version of Java:

```
sdk install 8.0.222-amzn
sdk use 8.0.222-amzn
java -version
```

## Download ElasticSearch version 1.7

```
cd ~/bin
curl -L -O https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-1.7.6.tar.gz
tar -xvf elasticsearch-1.7.6.tar.gz
```

## And start

```
cd elasticsearch-1.7.6/bin
elasticsearch
```

When starting in the future you will need to do `sdk use 8.0.222-amzn` first.

Links:

* [ElasticSearch 1.7 docs](https://www.elastic.co/guide/en/elasticsearch/reference/1.7/index.html)

