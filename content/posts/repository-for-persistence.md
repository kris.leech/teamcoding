---
title: "Repository for Persistence"
date: 2015-08-29
draft: true
---

# Repository for persistance

If we want to persist an entity to storage. 

Instead of calling `save`, `update_attributes`, `destroy` etc. on our ActiveRecord models we pass the entity to our repository and let it handle the persistance.

Why? Because it allows us to change the persistence mechanism without changing our entities.

It also allows us to persist non-ActiveRecord objects.

```ruby
Repository.put(study)
Repository.delete(study)
```

Internally Repository will use an adapter for the given entity. The adapter can be configured or inferred from the entity class.

In our case we will map all our entities to an `ActiveRecord` adapter which simply does `save(validate: false)` on the entity. In the future if we want to persist any entity with a different database we can add a new adapter.

The repository can just handles writes, but might also handle reads.

The repository also gives us a nice place to emit events to which an `AuditListener` can subscribe.

However the audit listener will only get the class/id of the entity, if the entity changes again before audit captures the state it will not be correct. Unless audit subscribes synchronously (i.e. blocking) and Repository is a thread-safe singleton. Even then it would be possible for the state to change before being recorded with multiple closely executed in different processes.

Another option would be to emit an event, after persisting, with all changed attributes but I’m not sure if all entities will allow this, e.g non-ActiveRecord. However ActiveModel does have support for [dirty attribute tracking](http://api.rubyonrails.org/classes/ActiveModel/Dirty.html) which could be used for PORO’s.
