---
title: "Thoughts on Service Api"
date: 2016-10-15
---

```ruby
# app initalization process
System = {}
System[:my_command] = MyCommand.new

# handle request from UI (e.g. in a controller)
System[:my_command].call(params) do
  on(:success) { }
  on(failure) { }
end
```

