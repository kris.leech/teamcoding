---
title: "Hi-res on Jessie"
date: 2016-10-22
---

Upgrading Debian kernel to gain support for Intel graphics card.

After installing Debian Jessie with the MATE desktop environment my monitor (Dell) was not detected and I could only select a resolution of 1024x768. I have an on-board graphics card connected via HDMI.

Manually adding a [new video mode](https://wiki.archlinux.org/index.php/xrandr) using `xrandr` did not help. I tried many other things too.

On the same machine I also had a Ubuntu install, by booting in to Ubuntu I was able to see which graphics card I had:

```
lspci -knn | grep -A4 VGA
00:02.0 VGA compatible controller [0300]: Intel Corporation Sky Lake Integrated Graphics [8086:1912] (rev 06)
	DeviceName:  Onboard IGD
	Subsystem: ASUSTeK Computer Inc. Skylake Integrated Graphics [1043:8694]
	Kernel driver in use: i915_bpo
	Kernel modules: i915_bpo
```

On Debian it was being reported as only, "Intel Corporation Device" and the Kernel module was reported as "xhci_hcd".

The kernel on Ubuntu was 4.4 and on Debian 3.16. I decided to update the kernel, this was suprisingly stright forward.

I knew this could result in an unbootable system. I have two drives, one with the OS's and another with my data. I knew that if all was to go horribly wrong I could just reinstall over the OS drive, mount the data drive and be back to work in next to no time. With that said...

The first step is to [add backports](https://backports.debian.org/Instructions/) to apt sources by adding `deb http://ftp.debian.org/debian jessie-backports main` to `/etc/apt/sources.list`.

And then install the latest 64 bit kernel:

```
apt-get install -t jessie-backports linux-image-amd64
```

`linux-image-amd64` is a meta-package and installed the 64 bit kernel, 4.7.

This is safe, the existing kernel is not replaced and will still be used when rebooting.

Linux kernels are 100% backwards compatible so there was no need to reinstall any software. It _should_ just work.

Finally (if only) I needed to [add the newly installed kernel as a bootable option in GRUB bootloader](http://unix.stackexchange.com/questions/311315/debian-installed-kernel-not-shown-in-grub):

```
update-grub
install-grub /dev/XXX
```

`update-grub` generates `/boot/grub/grub.cnf` and `install-grub` makes it active.

XXX is the device which has the boot partition, in my case it was nvme0n1 since I have an M.2 Drive. If you have any other type of drive it is more likely to be `sdX`, e.g. `sda1` if you only have one drive. If you have multiple drives use the "Drives" app in Ubunut or install `gparted` to check. You will see a partition at the beggining which has the type EFI and is mounted at `/boot/efi`. Another option is to run `grub-probe -t device /boot/grub`.

After rebooting and choosing Debian with kernel 4.7 from the GRUB menu I got a blank screen with a cursor flashing in the top left. Pressing the power button gracefully powered down.

The solution, thanks to [this hint](https://community.linuxmint.com/tutorial/view/842), was to boot without setting the video mode. To do this I placed my cursor over the Debian option in GRUB and pressed `e` to edit the boot options.

I then added `nomodeset` at the end of the line starting `linux` and pressed F10 to boot.

I got a desktop and confirmed using `uname -r` I was running the kernel 4.7 - Progress!

After this I installed the "Intel graphics drivers" from backports:

```
apt-get install -t jessie-backports xserver-xorg-video-intel
```

After a reboot I had a resolution of 3440x1440. :)
