---
title: "Rescue Argument Oddity"
date: 2019-05-15
---


```ruby
require "bundler/inline"

gemfile(true) do
  source "https://rubygems.org"
  gem "rspec"
end

BoomError = Class.new(StandardError)

RSpec.describe 'rescue arguments' do
  # passes, as expected
  describe 'no arguments' do
    it 'rescues StandardError' do
      expect do
        begin
          raise BoomError
        rescue
        end
      end.not_to raise_error
    end
  end

  # passes, as expected
  describe 'nil' do
    it 'rescues StandardError' do
      expect do
        begin
          raise BoomError
        rescue nil
        end
      end.to raise_error(TypeError)
    end
  end

  # fails, unexpected
  describe 'splat nil' do
    it 'rescues StandardError' do
      expect do
        begin
          raise BoomError
        rescue *nil
        end
      end.to raise_error(TypeError)
    end
  end
end
```

