---
title: "Reproducing async bugs in tests"
date: 2018-07-20
---

Let's say we have an object which sends a notification asynchronously, e.g. via sidekiq, when a task
is created for a user.

How do we test what happens when the Task has been deleted before the async job
has been run.

We want to simulate a change in state happening before command is run.

Maybe we could use a callback to let the test hook in and change the state
before it runs.

```ruby
task = create_task
command = SendNotification.new
command.before { task.destroy  }
expect { command.call(id: task.id) }.not_to raise_error
```

In this case we could pass an `id` of a task which does not exist.
