---
title: "For Gotcha in Clojure"
date: 2018-03-16
---

This will not work as expected, no users will be created.

```clj
(defn populate-users []
  (when (empty? (select-users db))
    (for [user hashed_users]
             (insert-user db user))))
```

This is because `for` returns a lazy collection, it does not evaludate until it is realized.

To fix this you need to realize the returned collection with `doall`:

```clj
(defn populate-users []
  (when (empty? (select-users db))
    (doall (for [user hashed_users]
             (insert-user db user)))))
```

or you can swap out the `doall` / `for` combination and use `doseq` instead. The `doseq` function does not return anything and is thus intended as an indicator that we are performing side effects.



