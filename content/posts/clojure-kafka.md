---
title: "Clojure Kafka"
date: 2017-06-22
---

```clojure
(require 'kinsky.client)
(def p (kinsky.client/producer { :bootstrap.servers "localhost:9092" } (kinsky.client/keyword-serializer) (kinsky.client/json-serializer)))
(kinsky.client/send! p "my-topic" :what-is-a-keyword { :foo "bar" })
```

