---
title: "Sed Examples"
date: 2017-09-17
draft: true
---

## delete line starting with "SET"

```
sed -e '/^SET/ d;' mos_production_data_inserts.sql > mos_production_data_inserts_fixed.sql 
```

## substitute "timestamp" with "datetime"

```
sed -e 's/timestamp/datetime/' mos_production_data_inserts.sql > mos_production_data_inserts_fixed.sql 
```

## remove "without time zone


```
sed -e 's/without time zone //' mos_production_data_inserts.sql > mos_production_data_inserts_fixed.sql 
```

## delete line starting with "SET" *AND* remove "witout time zone"

```
sed -e '/^SET/ d; s/without time zone //;' mos_production_data_inserts.sql > mos_production_data_inserts_fixed.sql 
```

