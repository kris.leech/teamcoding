---
layout: post
title: "Random Stuff"
date: 2013-08-07
comments: true
published: true
categories: [random]
draft: true
---

```ruby
expect(File).to exist(output_path)
```

Background noise:

http://www.rainymood.com/
http://rainycafe.com/

The Future of Programming:

http://vimeo.com/71278954

The unix filesystem:

http://www.pathname.com/fhs/pub/fhs-2.3.html
