---
title: "CSV Formating Ruby"
date: 2018-10-04
---

I find myself working with CSV a lot, here is a short example showing how I
usually format CSV for presentation:

```ruby
header, *rows = CSV.read('my-data.csv')

# convert from Array<Array> to Array<Hash>
#
rows.map! { |row| Hash[header.zip(row)] }

rows.map do |row|
  row.transform_values(&method(:format_value))
end

def format_value(value)
  case value
  when Date
    value.strftime('%d/%m/%Y')
  when TrueClass
    "Yes"
  when FalseClass
    "No"
  when NilClass
    ""
  else
    value
  end
end
```

