---
title: "Convert Bool to Str in Clojure"
date: 2018-01-04
---

I wanted a function to convert given keys in a vector and maps from true/false to "yes"/"no".

`[{ :is_it true } { :is_it false }] ;; => [{ :is_it "Yes" } { :is_it "No" }]`

Note I use `boolean?` which was added to Clojure 1.9, it can be replaced with `(instance? Boolean bool)` in 1.8.

## Attempt 1

```clj
;; converts boolean (true/false) to text (yes/no)
(defn- boolean-to-text [bool]
  (when (boolean? bool)
    (if bool "Yes" "No")))

;; returns true if coll contains given value
(defn- seq-contains? [coll target] (some #(= target %) coll))

;; converts key values from true/false to yes/no in map
(defn- coerce-boolean-values-for-map [bool-keys record]
  (into {} (for [[k v] record] [k (if (seq-contains? bool-keys k) (boolean-to-text v) v)])))

;; converts key values from boolean to text for collection of maps
(defn coerce-boolean-values-for-coll [bool-keys coll]
  {:pre [(every? map? coll)]}
  (map (partial coerce-boolean-values-for-map bool-keys) coll))
  
(defn run-test []
  (if (= (coerce-boolean-values-for-coll [:is_it] [{:is_it true :name "Kris"}]) [{:is_it "Yes" :name "Kris"}])
    (println "PASSED")
    (println "FAILED")))
```

## Atempt 2

```clj
;; converts boolean (true/false) to text (yes/no)
(defn- boolean-to-text [bool]
  (when (boolean? bool)
    (if bool "Yes" "No")))

;; returns true if coll contains given value
(defn- seq-contains? [coll target] (some #(= target %) coll))

(defmulti coerce-boolean-keys (fn [_ x] (map? x)))

(defmethod coerce-boolean-keys true [keys m]
  (into {} (for [[k v] m] [k (if (seq-contains? keys k) (boolean-to-text v) v)])))

;; I could have passed a `fn` to `map`, but choose to use `partial`, no idea which is idiomatic.
(defmethod coerce-boolean-keys false [keys c] (map (partial coerce-boolean-keys keys) c))

(defn run-test []
  (if (= (coerce-boolean-keys [:is_it] [{:is_it true :name "Kris"}]) [{:is_it "Yes" :name "Kris"}])
    (println "PASSED")
    (println "FAILED")))
```

## Attempt 3

```clj
(defmulti coerce-boolean-keys (fn [_ x] (class x)))

(defmethod coerce-boolean-keys clojure.lang.PersistentArrayMap [keys m]
  (into {} (for [[k v] m] [k (if (seq-contains? keys k) (boolean-to-text v) v)])))

(defmethod coerce-boolean-keys clojure.lang.PersistentVector [keys v] (map (partial coerce-boolean-keys keys) v))

(defn run-test2 []
  (if (= (coerce-boolean-keys [:is_it] [{:is_it true :name "Kris"}]) [{:is_it "Yes" :name "Kris"}])
    (println "PASSED")
    (println "FAILED")))

;; this works but fails if a list instead of a vector is passed, this may or may not be a problem, but we could add a `defmethod` for a list:

(defmethod coerce-boolean-keys clojure.lang.PersistentList [keys l] (coerce-boolean-keys keys (vec l)))

;; since an empty list has a different class, we also need to add that:

(defmethod coerce-boolean-keys clojure.lang.PersistentList$EmptyList [_ l] l) ;; we can just return the given empty list.
```
