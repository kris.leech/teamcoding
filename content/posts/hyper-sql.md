---
title: "Hyper SQL"
date: 2017-01-23
---

Clojure JDBC HyperSQL (like SQLite but Java only so self contained) 

```clojure
(require '[clojure.java.jdbc :as j])

;; file
(j/execute! { :subprotocol "hsqldb" :subname "file:./db/testdb"} ["CREATE TABLE changes(id INT)"])

;; in memory
(j/execute! { :subprotocol "hsqldb" :subname "file:./db/testdb"} ["CREATE TABLE changes(id INT)"])
```

