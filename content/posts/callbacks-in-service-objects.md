---
title: "Callbacks in Service Objects"
date: 2018-10-29
---

Just a thought. In our library we have a command which we
would like to extend in the application. Normally I would use a pub/sub model
where the command would emit an event.

But maybe we could use callbacks.

```ruby
class CreateStudy
  extend ActiveModel::Callbacks
  define_model_callbacks :create

  def call(form)
    if form.valid?
      @study = Study.new(form.study_attributes)
      
      run_callbacks :create do
        @study.save!
      end
    end
  end
end
```

Using the callback we can add additional behaviour in our application

```ruby
class CreateStudy
  after_create :add_site
  
  private
  
  def add_site
    # ...    
  end
end
```
