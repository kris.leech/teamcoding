---
title: "Home Office Kit List"
date: 2014-10-16
draft: true
---

* Electronic Standup desk, £150 on ebay.
* iMac + MacAir
* Samson Micophone
* Headphones (cheap, I need to buy better ones)
* Griffin laptop stand
* TypeMatrix keyboard
* Logitech Mouse
* Varier kneeling chair
* Desk lamp from Ikea
* Plants
* Wall art
* Second hand Sofa 
* Cube from Ikea
* Filing cainet from charity shop
* Box files from Ikea
