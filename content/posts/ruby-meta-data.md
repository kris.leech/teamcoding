---
title: "Ruby Meta Data"
date: 2017-02-01
---

```ruby
module Meta
  def meta
    @meta ||= {}
  end
end

[String Hash Array].each { |klass| klass.include(Meta) }
  
name = "Kris"
name.meta[:tained] = false
name.meta[:tained] # => false
```

Meta data does not influence the value of an object, i.e. equality.

```ruby
name == "Kris" # => true
```

The classes themseleves can also have meta data:

```ruby
class Foo
  extend Meta
end

Foo.meta[:doc] = "Provides XYZ"
```

This can also be DSL'd:

```ruby
module DocString
  def self.included(base)
    base.include(Meta)
    base.extend(ClassMethods)
  end
  
  module ClassMethods
    def doc(text = nil)
      if text
        meta[:doc] = text
      else
        meta[:doc] || ""
      end
    end
  end
end

class Foo
  include DocString
  
  doc "Provides XYZ"
end

Foo.doc # => "Provides XYZ"
```

Now we have docs inside out REPL.

## Notes

It does not work with Integer or Symbol instances since they are frozen.


